// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyA5Q4w2ImU8-iMD7y5JzhdgbJZN8Mzq8Us',
    appId: '1:854200102253:web:5cc46a69411d00805f3e64',
    messagingSenderId: '854200102253',
    projectId: 'reddit-f8e14',
    authDomain: 'reddit-f8e14.firebaseapp.com',
    storageBucket: 'reddit-f8e14.appspot.com',
    measurementId: 'G-HG13WGY5WH',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBf89rJcxLhrK8uFdu60BVa0velXUo6bvw',
    appId: '1:854200102253:android:c47bb56c4707d1fc5f3e64',
    messagingSenderId: '854200102253',
    projectId: 'reddit-f8e14',
    storageBucket: 'reddit-f8e14.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDCXg_CEqQ8yd_UU05g612SYRi2lZeQDCY',
    appId: '1:854200102253:ios:5c097f654a94a25c5f3e64',
    messagingSenderId: '854200102253',
    projectId: 'reddit-f8e14',
    storageBucket: 'reddit-f8e14.appspot.com',
    iosClientId: '854200102253-gqv4t5pg3hrod3k15k20j5atd874in8e.apps.googleusercontent.com',
    iosBundleId: 'com.example.redditTutorial',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyDCXg_CEqQ8yd_UU05g612SYRi2lZeQDCY',
    appId: '1:854200102253:ios:5c097f654a94a25c5f3e64',
    messagingSenderId: '854200102253',
    projectId: 'reddit-f8e14',
    storageBucket: 'reddit-f8e14.appspot.com',
    iosClientId: '854200102253-gqv4t5pg3hrod3k15k20j5atd874in8e.apps.googleusercontent.com',
    iosBundleId: 'com.example.redditTutorial',
  );
}
