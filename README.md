Kriteria Penilaian

1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

## Use case user

| No | Use Case | Deskripsi | Prioritas | Kelas Terkait |
|----|----------|------------|-----------|---------------|
| 1 | Registrasi Akun | Pengguna dapat mendaftar untuk membuat akun baru di Reddit. | Tinggi | Pengguna, Keamanan |
| 2 | Login | Pengguna dapat masuk ke akun Reddit mereka menggunakan informasi login yang valid. | Tinggi | Pengguna, Keamanan |
| 3 | Mencari Konten | Pengguna dapat mencari konten yang spesifik di Reddit berdasarkan kata kunci atau topik tertentu. | Tinggi | Pengguna, Konten |
| 4 | Menyimpan Postingan | Pengguna dapat menyimpan postingan yang menarik untuk dibaca atau dilihat nanti. | Sedang | Pengguna, Konten |
| 5 | Memberikan Upvote dan Downvote | Pengguna dapat memberikan suara positif (upvote) atau negatif (downvote) pada postingan atau komentar yang mereka sukai atau tidak sukai. | Tinggi | Pengguna, Interaksi |
| 6 | Mengomentari Postingan | Pengguna dapat mengomentari postingan dan berinteraksi dengan pengguna lainnya dalam thread komentar. | Tinggi | Pengguna, Interaksi |
| 7 | Mengikuti Subreddit | Pengguna dapat mengikuti subreddit-subreddit yang menarik bagi mereka untuk mendapatkan konten terkini dari subreddit tersebut. | Sedang | Pengguna, Konten |
| 8 | Mengirim Pesan Pribadi | Pengguna dapat mengirim pesan pribadi kepada pengguna lain di Reddit. | Sedang | Pengguna, Interaksi |
| 9 | Membuat Postingan | Pengguna dapat membuat postingan baru di subreddit-subreddit yang relevan. | Tinggi | Pengguna, Konten |
| 10 | Melihat Aktivitas Pengguna | Pengguna dapat melihat aktivitas mereka sendiri, seperti postingan terakhir, komentar, dan suara yang mereka berikan. | Sedang | Pengguna, Konten |
| 11 | Mengikuti Pengguna | Pengguna dapat mengikuti pengguna lain di Reddit untuk melihat konten yang mereka bagikan. | Sedang | Pengguna, Konten |
| 12 | Mengatur Notifikasi | Pengguna dapat mengatur preferensi notifikasi untuk menerima pemberitahuan tentang aktivitas di Reddit. | Sedang | Pengguna, Pengaturan |
| 13 | Memblokir Pengguna | Pengguna dapat memblokir pengguna lain di Reddit untuk menghentikan interaksi atau konten dari pengguna tersebut. | Sedang | Pengguna, Keamanan |




## Use case manajemen perusahaan

Berikut adalah daftar Use Case Manajemen Perusahaan dalam format tabel Markdown:


| No | Use Case | Deskripsi | Prioritas |
|----|----------|------------|-----------|
| 1 | Pengelolaan Data Karyawan | Manajemen perusahaan dapat menggunakan sistem untuk mengelola data karyawan, termasuk informasi personal, riwayat pekerjaan, kinerja, dan detail kontak. | Tinggi |
| 2 | Rekrutmen dan Perekrutan | Manajemen perusahaan dapat menggunakan sistem untuk mengelola proses rekrutmen, termasuk pengiklanan lowongan, penerimaan aplikasi, wawancara, dan seleksi calon karyawan. | Tinggi |
| 3 | Manajemen Performa Karyawan | Manajemen perusahaan dapat menggunakan sistem untuk mengelola dan mengevaluasi kinerja karyawan, termasuk penetapan tujuan, pelacakan kemajuan, umpan balik, dan penilaian kinerja. | Tinggi |
| 4 | Pengelolaan Jadwal dan Absensi | Manajemen perusahaan dapat menggunakan sistem untuk mengelola jadwal kerja karyawan, memantau absensi, cuti, dan keterlambatan, serta menghasilkan laporan kehadiran. | Sedang |
| 5 | Pengembangan Karyawan | Manajemen perusahaan dapat menggunakan sistem untuk merencanakan dan melacak program pengembangan karyawan, termasuk pelatihan, pengembangan keterampilan, dan program suksesi. | Sedang |
| 6 | Pengelolaan Gaji dan Kompensasi | Manajemen perusahaan dapat menggunakan sistem untuk mengelola proses penggajian, termasuk perhitungan gaji, penggajian otomatis, administrasi tunjangan, dan manajemen insentif. | Tinggi |
| 7 | Manajemen Hubungan Karyawan | Manajemen perusahaan dapat menggunakan sistem untuk memfasilitasi komunikasi dan kolaborasi antara karyawan, termasuk pengumuman perusahaan, diskusi, dan berbagi informasi. | Sedang |
| 8 | Manajemen Proyek dan Tugas | Manajemen perusahaan dapat menggunakan sistem untuk mengelola proyek dan tugas, termasuk alokasi sumber daya, penugasan tugas, pelacakan kemajuan, dan pengaturan tenggat waktu. | Sedang |
| 9 | Pengelolaan Pengeluaran dan Anggaran | Manajemen perusahaan dapat menggunakan sistem untuk mengelola pengeluaran, melacak anggaran, memproses klaim biaya, dan menghasilkan laporan keuangan terkait. | Sedang |
| 10 | Manajemen Komunikasi Internal | Manajemen perusahaan dapat menggunakan sistem untuk memfasilitasi komunikasi internal, termasuk pengiriman pesan, pengumuman, kolaborasi tim, dan pembuatan grup diskusi. | Sedang |


## Use case direksi perusahaan (dashboard, monitoring, analisis)

| No | Use Case | Deskripsi | Prioritas |
|----|----------|------------|-----------|
| 1 | Dashboard Direksi | Direksi perusahaan dapat mengakses dashboard yang menyajikan ringkasan data penting, termasuk laporan keuangan, kinerja operasional, dan metrik kunci lainnya. | Tinggi |
| 2 | Monitoring Kinerja Perusahaan | Direksi perusahaan dapat menggunakan sistem untuk memantau kinerja perusahaan secara real-time, termasuk pendapatan, laba, pangsa pasar, dan indikator kinerja utama lainnya. | Tinggi |
| 3 | Analisis Pasar dan Persaingan | Direksi perusahaan dapat menggunakan sistem untuk menganalisis tren pasar, perilaku pelanggan, dan persaingan dalam industri, sehingga dapat mengambil keputusan strategis yang informasional. | Tinggi |
| 4 | Manajemen Risiko | Direksi perusahaan dapat menggunakan sistem untuk mengidentifikasi, mengukur, dan mengelola risiko yang dihadapi perusahaan, termasuk risiko keuangan, operasional, dan reputasi. | Sedang |
| 5 | Pelaporan Keuangan | Direksi perusahaan dapat menghasilkan laporan keuangan yang akurat dan tepat waktu, termasuk laporan laba rugi, neraca, arus kas, dan catatan atas keuangan. | Tinggi |
| 6 | Evaluasi Proyek dan Investasi | Direksi perusahaan dapat menggunakan sistem untuk mengevaluasi proyek-proyek baru, investasi, dan strategi bisnis potensial untuk memastikan keputusan yang tepat berdasarkan analisis risiko dan keuntungan. | Sedang |
| 7 | Pengelolaan Rencana Strategis | Direksi perusahaan dapat menggunakan sistem untuk mengelola rencana strategis perusahaan, termasuk penetapan visi, misi, tujuan jangka panjang, serta pemantauan dan pelaporan kemajuan. | Sedang |
| 8 | Pengambilan Keputusan Berbasis Data | Direksi perusahaan dapat menggunakan sistem untuk mengumpulkan, menganalisis, dan mempresentasikan data yang relevan guna mendukung pengambilan keputusan yang informasional dan akurat. | Tinggi |
| 9 | Komunikasi dan Kolaborasi Direksi | Direksi perusahaan dapat menggunakan sistem untuk berkomunikasi dan berkolaborasi secara efisien, termasuk pertemuan virtual, berbagi dokumen, dan diskusi untuk mendukung proses pengambilan keputusan. | Sedang |
| 10 | Manajemen Keberlanjutan | Direksi perusahaan dapat menggunakan sistem untuk mengelola aspek keberlanjutan perusahaan, termasuk lingkungan, sosial, dan tata kelola yang bertanggung jawab, serta melaporkan dampak keberlanjutan. | Sedang |



## 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

![Deskripsi Gambar](ss/1.Class%20Diagram.png)

## 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

````
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:reddit_tutorial/core/constants/constants.dart';
import 'package:reddit_tutorial/core/constants/firebase_constants.dart';
import 'package:reddit_tutorial/core/failure.dart';
import 'package:reddit_tutorial/core/providers/firebase_providers.dart';
import 'package:reddit_tutorial/core/type_defs.dart';
import 'package:reddit_tutorial/models/user_model.dart';

final authRepositoryProvider = Provider(
  (ref) => AuthRepository(
    firestore: ref.read(firestoreProvider),
    auth: ref.read(authProvider),
    googleSignIn: ref.read(googleSignInProvider),
  ),
);

class AuthRepository {
  final FirebaseFirestore _firestore;
  final FirebaseAuth _auth;
  final GoogleSignIn _googleSignIn;

  AuthRepository({
    required FirebaseFirestore firestore,
    required FirebaseAuth auth,
    required GoogleSignIn googleSignIn,
  })  : _auth = auth,
        _firestore = firestore,
        _googleSignIn = googleSignIn;

  CollectionReference get _users => _firestore.collection(FirebaseConstants.usersCollection);

  Stream<User?> get authStateChange => _auth.authStateChanges();

  FutureEither<UserModel> signInWithGoogle(bool isFromLogin) async {
    try {
      UserCredential userCredential;
      if (kIsWeb) {
        GoogleAuthProvider googleProvider = GoogleAuthProvider();
        googleProvider.addScope('https://www.googleapis.com/auth/contacts.readonly');
        userCredential = await _auth.signInWithPopup(googleProvider);
      } else {
        final GoogleSignInAccount? googleUser = await _googleSignIn.signIn();

        final googleAuth = await googleUser?.authentication;

        final credential = GoogleAuthProvider.credential(
          accessToken: googleAuth?.accessToken,
          idToken: googleAuth?.idToken,
        );

        if (isFromLogin) {
          userCredential = await _auth.signInWithCredential(credential);
        } else {
          userCredential = await _auth.currentUser!.linkWithCredential(credential);
        }
      }

      UserModel userModel;

      if (userCredential.additionalUserInfo!.isNewUser) {
        userModel = UserModel(
          name: userCredential.user!.displayName ?? 'No Name',
          profilePic: userCredential.user!.photoURL ?? Constants.avatarDefault,
          banner: Constants.bannerDefault,
          uid: userCredential.user!.uid,
          isAuthenticated: true,
          karma: 0,
          awards: [
            'awesomeAns',
            'gold',
            'platinum',
            'helpful',
            'plusone',
            'rocket',
            'thankyou',
            'til',
          ],
        );
        await _users.doc(userCredential.user!.uid).set(userModel.toMap());
      } else {
        userModel = await getUserData(userCredential.user!.uid).first;
      }
      return right(userModel);
    } on FirebaseException catch (e) {
      throw e.message!;
    } catch (e) {
      return left(Failure(e.toString()));
    }
  }

  FutureEither<UserModel> signInAsGuest() async {
    try {
      var userCredential = await _auth.signInAnonymously();

      UserModel userModel = UserModel(
        name: 'Guest',
        profilePic: Constants.avatarDefault,
        banner: Constants.bannerDefault,
        uid: userCredential.user!.uid,
        isAuthenticated: false,
        karma: 0,
        awards: [],
      );

      await _users.doc(userCredential.user!.uid).set(userModel.toMap());

      return right(userModel);
    } on FirebaseException catch (e) {
      throw e.message!;
    } catch (e) {
      return left(Failure(e.toString()));
    }
  }

  Stream<UserModel> getUserData(String uid) {
    return _users.doc(uid).snapshots().map((event) => UserModel.fromMap(event.data() as Map<String, dynamic>));
  }

  void logOut() async {
    await _googleSignIn.signOut();
    await _auth.signOut();
  }
}

````



**Single Responsibility Principle (SRP)**

Kelas AuthRepository bertanggung jawab untuk menangani logika otentikasi dan otorisasi pengguna. Ia memiliki tanggung jawab yang terkait dengan manajemen pengguna, autentikasi melalui Google, dan mengakses data pengguna dari Firestore.


**Open/Closed Principle (OCP)**

Kelas AuthRepository dapat diperluas dengan mudah jika ada penambahan metode baru atau integrasi dengan sistem autentikasi lainnya. Namun, kita perlu berhati-hati dengan kohesi dan prinsip SRP saat memperluas kelas ini.

**Dependency Inversion Principle (DIP)**

Kelas AuthRepository menerapkan prinsip DIP dengan menggunakan dependensi injeksi melalui konstruktor. Dependensi pada kelas seperti FirebaseFirestore, FirebaseAuth, dan GoogleSignIn diinjeksikan melalui konstruktor, yang memungkinkan fleksibilitas dan pengujian yang lebih baik dengan mengganti implementasi dengan mock atau stub saat melakukan pengujian unit.

`````
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reddit_tutorial/core/common/error_text.dart';
import 'package:reddit_tutorial/core/common/loader.dart';
import 'package:reddit_tutorial/features/auth/controlller/auth_controller.dart';
import 'package:reddit_tutorial/firebase_options.dart';
import 'package:reddit_tutorial/models/user_model.dart';
import 'package:reddit_tutorial/router.dart';
import 'package:reddit_tutorial/theme/pallete.dart';
import 'package:routemaster/routemaster.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseAuth auth = FirebaseAuth.instance;
  auth.signOut();
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends ConsumerStatefulWidget {
  const MyApp({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _MyAppState();
}

class _MyAppState extends ConsumerState<MyApp> {
  UserModel? userModel;

  void getData(WidgetRef ref, User data) async {
    userModel = await ref
        .watch(authControllerProvider.notifier)
        .getUserData(data.uid)
        .first;
    ref.read(userProvider.notifier).update((state) => userModel);
  }

  @override
  Widget build(BuildContext context) {
    return ref.watch(authStateChangeProvider).when(
          data: (data) => MaterialApp.router(
            debugShowCheckedModeBanner: false,
            title: 'Reddit Tutorial',
            theme: ref.watch(themeNotifierProvider),
            routerDelegate: RoutemasterDelegate(
              routesBuilder: (context) {
                if (data != null) {
                  getData(ref, data);
                  if (userModel != null) {
                    return loggedInRoute;
                  }
                }
                return loggedOutRoute;
              },
            ),
            routeInformationParser: const RoutemasterParser(),
          ),
          error: (error, stackTrace) => ErrorText(error: error.toString()),
          loading: () => const Loader(),
        );
  }
}

`````

**Single Responsibility Principle (SRP):**

Kelas MyApp bertanggung jawab untuk menginisialisasi aplikasi dan menampilkan widget utama. Ini mengatur konfigurasi awal dan memastikan pengikatan provider dan route yang sesuai.
Kelas _MyAppState bertanggung jawab untuk memperbarui data pengguna dan membangun widget tampilan berdasarkan status otentikasi. Ini juga bertanggung jawab untuk mengambil data pengguna dan memperbarui state pengguna melalui provider.

**Open/Closed Principle (OCP):**

Kelas MyApp dapat diperluas dengan mudah untuk menambahkan konfigurasi tambahan atau logika terkait inisialisasi aplikasi tanpa memodifikasi kode yang ada.

**Dependency Inversion Principle (DIP):**

Terdapat penggunaan prinsip DIP melalui penggunaan flutter_riverpod untuk melakukan dependensi injeksi pada kelas MyApp dan _MyAppState. Ini memungkinkan fleksibilitas dalam mengganti implementasi provider saat melakukan pengujian atau perubahan kebutuhan.

````
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reddit_tutorial/core/common/error_text.dart';
import 'package:reddit_tutorial/core/common/loader.dart';
import 'package:reddit_tutorial/core/utils.dart';
import 'package:reddit_tutorial/features/community/controller/community_controller.dart';
import 'package:reddit_tutorial/features/post/controller/post_controller.dart';
import 'package:reddit_tutorial/models/community_model.dart';
import 'package:reddit_tutorial/responsive/responsive.dart';
import 'package:reddit_tutorial/theme/pallete.dart';

class AddPostTypeScreen extends ConsumerStatefulWidget {
  final String type;
  const AddPostTypeScreen({
    super.key,
    required this.type,
  });

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _AddPostTypeScreenState();
}

class _AddPostTypeScreenState extends ConsumerState<AddPostTypeScreen> {
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final linkController = TextEditingController();
  File? bannerFile;
  Uint8List? bannerWebFile;
  List<Community> communities = [];
  Community? selectedCommunity;

  @override
  void dispose() {
    super.dispose();
    titleController.dispose();
    descriptionController.dispose();
    linkController.dispose();
  }

  void selectBannerImage() async {
    final res = await pickImage();

    if (res != null) {
      if (kIsWeb) {
        setState(() {
          bannerWebFile = res.files.first.bytes;
        });
      }
      setState(() {
        bannerFile = File(res.files.first.path!);
      });
    }
  }

  void sharePost() {
    if (widget.type == 'image' &&
        (bannerFile != null || bannerWebFile != null) &&
        titleController.text.isNotEmpty) {
      ref.read(postControllerProvider.notifier).shareImagePost(
            context: context,
            title: titleController.text.trim(),
            selectedCommunity: selectedCommunity ?? communities[0],
            file: bannerFile,
            webFile: bannerWebFile,
          );
    } else if (widget.type == 'text' && titleController.text.isNotEmpty) {
      ref.read(postControllerProvider.notifier).shareTextPost(
            context: context,
            title: titleController.text.trim(),
            selectedCommunity: selectedCommunity ?? communities[0],
            description: descriptionController.text.trim(),
          );
    } else if (widget.type == 'link' &&
        titleController.text.isNotEmpty &&
        linkController.text.isNotEmpty) {
      ref.read(postControllerProvider.notifier).shareLinkPost(
            context: context,
            title: titleController.text.trim(),
            selectedCommunity: selectedCommunity ?? communities[0],
            link: linkController.text.trim(),
          );
    } else {
      showSnackBar(context, 'Please enter all the fields');
    }
  }

  @override
  Widget build(BuildContext context) {
    final isTypeImage = widget.type == 'image';
    final isTypeText = widget.type == 'text';
    final isTypeLink = widget.type == 'link';
    final currentTheme = ref.watch(themeNotifierProvider);
    final isLoading = ref.watch(postControllerProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text('Post ${widget.type}'),
        actions: [
          TextButton(
            onPressed: sharePost,
            child: const Text('Share'),
          ),
        ],
      ),
      body: isLoading
          ? const Loader()
          : Responsive(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    TextField(
                      controller: titleController,
                      decoration: const InputDecoration(
                        filled: true,
                        hintText: 'Enter Title here',
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(18),
                      ),
                      maxLength: 30,
                    ),
                    const SizedBox(height: 10),
                    if (isTypeImage)
                      GestureDetector(
                        onTap: selectBannerImage,
                        child: DottedBorder(
                          borderType: BorderType.RRect,
                          radius: const Radius.circular(10),
                          dashPattern: const [10, 4],
                          strokeCap: StrokeCap.round,
                          color: currentTheme.textTheme.bodyText2!.color!,
                          child: Container(
                            width: double.infinity,
                            height: 130,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: bannerWebFile != null
                                ? Image.memory(bannerWebFile!)
                                : bannerFile != null
                                    ? Image.file(bannerFile!)
                                    : const Center(
                                        child: Icon(
                                          Icons.camera_alt_outlined,
                                          size: 40,
                                        ),
                                      ),
                          ),
                        ),
                      ),
                    if (isTypeText)
                      TextField(
                        controller: descriptionController,
                        decoration: const InputDecoration(
                          filled: true,
                          hintText: 'Enter Description here',
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.all(18),
                        ),
                        maxLines: 5,
                      ),
                    if (isTypeLink)
                      TextField(
                        controller: linkController,
                        decoration: const InputDecoration(
                          filled: true,
                          hintText: 'Enter link here',
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.all(18),
                        ),
                      ),
                    const SizedBox(height: 20),
                    const Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Select Community',
                      ),
                    ),
                    ref.watch(userCommunitiesProvider).when(
                          data: (data) {
                            communities = data;

                            if (data.isEmpty) {
                              return const SizedBox();
                            }

                            return DropdownButton(
                              value: selectedCommunity ?? data[0],
                              items: data
                                  .map(
                                    (e) => DropdownMenuItem(
                                      value: e,
                                      child: Text(e.name),
                                    ),
                                  )
                                  .toList(),
                              onChanged: (val) {
                                setState(() {
                                  selectedCommunity = val;
                                });
                              },
                            );
                          },
                          error: (error, stackTrace) => ErrorText(
                            error: error.toString(),
                          ),
                          loading: () => const Loader(),
                        ),
                  ],
                ),
              ),
            ),
    );
  }
}
````

Berikut adalah penjelasan penerapan poin-poin SOLID Design Principle dalam kode `AddPostTypeScreen` yang Anda berikan:

**1. Single Responsibility Principle (SRP)**
   - Kelas `AddPostTypeScreen` bertanggung jawab untuk mengatur tampilan dan perilaku layar penambahan posting berdasarkan tipe posting yang dipilih.
   - Metode-metode yang ada dalam kelas ini berfokus pada fungsi-fungsi spesifik seperti memilih gambar, berbagi posting, mengambil data komunitas, dll.

**2. Open/Closed Principle (OCP)**
   - Kelas `AddPostTypeScreen` dapat diperluas dengan mudah untuk menambahkan jenis posting baru tanpa mengubah kode yang ada. Misalnya, jika ingin menambahkan jenis posting video, cukup menambahkan kode untuk jenis posting tersebut tanpa memodifikasi bagian kode yang ada.

**3. Dependency Inversion Principle (DIP)**
   - Kelas `AddPostTypeScreen` menerapkan prinsip DIP dengan menggunakan `flutter_riverpod` untuk mendapatkan dependensi seperti `postControllerProvider` dan `themeNotifierProvider`. Ini memungkinkan fleksibilitas dalam mengganti implementasi dan pengujian dengan menggunakan dependensi yang sesuai.



## 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih
#### DESAIN PATTERN BUILDER
```
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:reddit_tutorial/models/post_model.dart';

class PostCard extends StatelessWidget {
  final Post post;

  const PostCard({Key? key, required this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PostCardBuilder()
      .setPost(post)
      .build(context);
  }
}

class PostCardBuilder {
  late Post _post;

  PostCardBuilder setPost(Post post) {
    _post = post;
    return this;
  }

  Widget build(BuildContext context) {
    final isTypeImage = _post.type == 'image';
    final isTypeText = _post.type == 'text';
    final isTypeLink = _post.type == 'link';

    // Build the card widget here based on the post data

    return Card(
      // Widget configuration based on post data
    );
  }
}

```
Dalam contoh di atas, kita menggunakan desain pattern Builder untuk memisahkan proses konstruksi objek `PostCard` dari representasi akhirnya. Pola desain Builder memungkinkan kita untuk membangun objek dengan urutan yang fleksibel dan mengatur parameter serta konfigurasi dengan lebih rinci.
Pertama, kita memiliki kelas `PostCard` yang merupakan subclass dari `StatelessWidget`. Kelas ini menerima parameter `post` yang merupakan objek `Post` yang akan digunakan untuk membangun kartu postingan (`PostCard`). 
Kemudian, kita memiliki kelas `PostCardBuilder` yang bertanggung jawab untuk membangun objek `PostCard` dengan parameter dan konfigurasi yang diberikan. Pada kelas `PostCardBuilder`, kita menggunakan metode `setPost()` untuk mengatur objek `Post` yang akan digunakan dalam pembangunan `PostCard`.

Metode `setPost()` mengatur nilai `_post` dengan objek `Post` yang diterima sebagai argumen dan mengembalikan instance `PostCardBuilder` itu sendiri. Ini memungkinkan kita untuk memanggil metode `setPost()` secara berantai.

Selanjutnya, kita memiliki metode `build()` yang menerima `BuildContext` sebagai argumen. Di dalam metode `build()`, kita menggunakan nilai `_post` yang telah diatur sebelumnya untuk melakukan pembangunan kartu postingan (`PostCard`) yang sesuai dengan data yang diberikan.

Pada implementasi ini, kita dapat menggunakan `PostCardBuilder` untuk mengatur parameter dan konfigurasi sebelum membangun objek `PostCard` dengan memanggil metode `build()`. Ini memberikan fleksibilitas kepada pengguna untuk mengatur dan mengonfigurasi objek `PostCard` dengan lebih rinci.

Terima kasih atas pertanyaannya, dan harapannya ini memberikan penjelasan lebih rinci tentang implementasi desain pattern Builder dalam contoh kodingan `PostCard`. Jika ada pertanyaan lebih lanjut, silakan beri tahu saya.

## 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
````
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:google_sign_in/google_sign_in.dart';

final firestoreProvider = Provider((ref) => FirebaseFirestore.instance);
final authProvider = Provider((ref) => FirebaseAuth.instance);
final storageProvider = Provider((ref) => FirebaseStorage.instance);
final googleSignInProvider = Provider((ref) => GoogleSignIn());
````

Kode tersebut merupakan inisialisasi koneksi ke berbagai layanan Firebase yang terkait dengan database dan penyimpanan. Berikut adalah penjelasan masing-masing provider:

1. `firestoreProvider`: Menginisialisasi koneksi ke layanan Cloud Firestore menggunakan `FirebaseFirestore.instance`. Ini memungkinkan aplikasi untuk berinteraksi dengan database Firestore, menyimpan dan mengambil data.

2. `authProvider`: Menginisialisasi koneksi ke layanan Firebase Authentication menggunakan `FirebaseAuth.instance`. Ini memungkinkan aplikasi untuk mengautentikasi pengguna dan mengelola informasi pengguna terkait otentikasi.

3. `storageProvider`: Menginisialisasi koneksi ke layanan Firebase Storage menggunakan `FirebaseStorage.instance`. Ini memungkinkan aplikasi untuk menyimpan dan mengambil file dan objek biner lainnya ke dan dari penyimpanan Firebase.

4. `googleSignInProvider`: Menginisialisasi koneksi ke layanan Google Sign-In menggunakan `GoogleSignIn()`. Ini memungkinkan pengguna untuk melakukan otentikasi dengan akun Google mereka.

Dengan menggunakan provider ini, aplikasi dapat mengakses dan menggunakan fitur-fitur yang disediakan oleh layanan Firebase terkait database, otentikasi, dan penyimpanan.


## 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya


## 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

## 1.Login
![1.Login](ss/1.Login.png)

## 2.Home
![2.Home](ss/2.Home.png)

## 3.Menu Upload
![3.Menu Upload ](ss/3.Menu_Upload.png)

## 4.Menu Posting
![](ss/4.Menu_post_image.png)

## 5.Select Photo
![](ss/5.select_photo.png)

## 6.Posting Succes
![](ss/7.succes_post.png)

## 7. Post Text
![](ss/6.post_text.png)

## 8.Post Link
![](ss/8.post_link.png)

## 9.Reward
![](ss/9.voteup%20&%20votedown.png)

## 10. Profile
![](ss/10.profile.png)

## 11. Edit Profile
![](ss/11.editprofile.png)

## 12.Comment
![](ss/12.comment.png)

## 13. Create Community
![](ss/13.CreatCommunity.png)

## 14. Name Community
![](ss/14.NameCommunity.png)

## 15. Light Mode
![](ss/15.lightMode.png)

## 16. Dark Mode
![](ss/16.DarkMode.png)

## 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

Dart - Flutter

## 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube



![](ss/17.yt.png)


**Video Demo Project UAS**
   [Link Video](https://youtu.be/lY1Ku_TBBmM)

## 10. BONUS !!! Mendemonstrasikan penggunaan Machine Learning